### Session 1: HTML, CSS, JavaScript

I believe the most valuable skill I could impart to folks beginning their journey into web development may be the art of troubleshooting. I hope the time we spent walking through the browser tools will prove useful as you continue to practice individually.

##### Sublime Text

Install packages.  Browse at https://packagecontrol.io

* CSS3
* CSSTidy
* HTML5
* HTMLBeautify
* Javascript Beautify
* JsFormat

Advanced plugins

* Hayaku
* Markdown Preview

Why not get started on your portfolio site before we meet again on Tuesday? If you hit a stumbling block, we can work through it together. We will also be introducing some additional skills that you may want to apply. Find a couple sites you like and peek at the code to see what makes them tick.
