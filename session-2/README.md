### Session 2: Forms & Flex Box

I have pushed my sample intro site to Bitbucket.  You can see it here:
https://bitbucket.org/ianmouton/dci-cohort-6/src/master/session-2/

If you're not familiar with Bitbucket or version control tools in general, now's a great time to learn!  These tools allow you to keep versions of your work stored safely.  When you make a bad change, you simply reverse in time to the last good version.  These tools also allow several people to coordinate changes to a common set of files without destroying each other's work.  Quite handy!

Version control systems (VCS) are commonplace in software development, but they are more rare in other contexts, including graphic design.  Ask Chris about his versioning scheme!  The fact that he had/has one is good...but it was very manual.


##### Session 2 Review

* Forms!  Everything goes inside the <form> tag.  Use javascript to custom process the data
* Javascript: compare the raw javascript with jquery.  Another benefit of jquery is how it insulates you from the different browser "personalities".  Write it once, run it everywhere.
* Custom fonts.  A couple examples are found within my shared files.  Check out http://fonts.googleapis.com/css?family=Open+Sans for more examples of loading local and remote fonts.
* Remember your troubleshooting tools!  Use the browser console to understand why your page doesn't look / behave as desired.
* Flex box.  Check out https://css-tricks.com/snippets/css/a-guide-to-flexbox/ for a good guide
* Check out https://code.jquery.com/ as a place to grab jquery builds.  Remember that by using a common library location (think CDN) you can optimize use of your browser cache and speed up your web pages.
* Codepen is a nifty way to explore HTML + CSS + Javascript and preview the result.  A couple flex box examples are:
	* https://codepen.io/team/css-tricks/pen/EKEYob
	* https://codepen.io/team/css-tricks/pen/YqaKYR

Reply with any questions or issues.  Come prepared next session to cover jquery + css!

~End of transmission~