<?php
	get_header();
?>

	<header class="page-header">
		<h2 class="page-title"><?php _e( 'Posts', 'bigbird' ); ?></h2>
	</header>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		</main>
	</div>
	<div id="sidebar" style="background-color:#aaaaaa; padding:10px">
		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>