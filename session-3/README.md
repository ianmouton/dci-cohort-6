### Session 3: JQuery & CSS

##### Decompose magdesign.com

Let's see how a production website is built!  We can use the browser console to 
execute JQuery commands and "hack" the site.

http://magdesign.com/assets/js/master.min.js

* Paste into a new file
* Command Palette -> Format: JavaScript

http://magdesign.com/assets/css/master.css

* Responsive!  See what happens when you shrink your browser window < 570px wide

Some tools used on the site:

* Bower: https://bower.io/
* Modernizr: https://modernizr.com/
* Typekit
* Google Analytics


##### JQuery basics

* Write once, run everwhere.  Browser compatibility is handled for you
* Performance.  JQuery code is optimized for speed
* JQuery is an extensible platform.  Checkout https://jqueryui.com
* Uses selectors, just like CSS
* Is quite readable: if ($("nav.mobile").is(":hidden")) { $("nav.mobile").show(); }


##### Exercise time!

Use jquery in a simple example:

+ Create an element with class "bunny"
+ Create a hidden element with class "fox"
+ Register a click handler on the ".bunny" element AFTER the page is ready
+ Have the ".bunny" click handler toggle the ".fox" element


##### Learn more

JQuery, like HTML, CSS, and JavaScript is a language.  And just like a spoken language,
your vocabulary will improve with usage, over time.  Use the api docs to improve your
JQuery vocabulary.  Explore the learning guide to familiarize yourself with the capabilities.

Discover JQuery through their docs

* https://jquery.com
* https://api.jquery.com/
* https://learn.jquery.com/