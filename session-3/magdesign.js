/*
	See how the "click handler" is attached to ".nav-mobile-trigger" ~after~ 
	the page has been loaded by the browser (the DOM has been parsed, so all 
	HTML tags on the page are recognized)

	Other points of interest
	* e.preventDefault() tells the browser to not perform the default click action (load a page)
	* spin_hamburger() play with this in the browser console to see what it does!
	* $('...').velocity is a jquery function which controls the speed of a function. See https://api.jquery.com
	* $('...').slideDown is a jquery function to show an element.  See https://api.jquery.com
	* $('...').slideUp is a jquery function to hide an element.  See https://api.jquery.com
*/
jQuery(document).ready(function($) {
	$(".nav-mobile-trigger").click(function(e) {
		e.preventDefault(), $("nav.mobile").is(":hidden") ? $("nav.mobile").velocity("slideDown", {
			duration: 200
		}) : $("nav.mobile").velocity("slideUp", {
			duration: 200
		}), spin_hamburger()
	}), $("#contact-button").click(function(e) {
		e.preventDefault(), submit_contact_form()
	})
})



/*
	See how Typekit handles errors gracefully.  This code says:
	try to load Typekit, and ignore any errors (carry on)
*/
try {
	Typekit.load();
} catch (e) {}



/*
	Google Analytics loader

	Note the clever way the function arguments spell "boiler".  See how
	these translate to: window, document, 'script', 'ga'

	This function 
	1. attaches a function named 'ga' to the window
	2. creates a new <script> tag with src = www.google-analytics.com/analytics.js
	3. inserts the new <script> tag in front of the first <script> tag defined on the page

	Once the 'ga' function is defined, it's called twice to register 2 events:
	1. "create" establishes a page session
	2. "send" sends Google Analytics a "pageview" event
*/
(function(b, o, i, l, e, r) {
	b.GoogleAnalyticsObject = l;
	b[l] || (b[l] =
		function() {
			(b[l].q = b[l].q || []).push(arguments)
		});
	b[l].l = +new Date;
	e = o.createElement(i);
	r = o.getElementsByTagName(i)[0];
	e.src = '//www.google-analytics.com/analytics.js';
	r.parentNode.insertBefore(e, r)
}(window, document, 'script', 'ga'));
ga('create', 'UA-11364391-1');
ga('send', 'pageview');