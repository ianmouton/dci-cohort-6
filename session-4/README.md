### Session 4: WordPress

### Install WordPress on your computer

##### Download software

Download MAMP (for Mac & Windows) from https://www.mamp.info/en/downloads/.  
Just look for the big orange "Download" button.

Download the latest version of wordpress from https://wordpress.org/.
Click the blue "Download WordPress" at the top right.

While these are downloading, let's load 1 more website: https://api.wordpress.org/secret-key/1.1/salt/.  This will give us some good authentication keys for use later.

##### Install MAMP

The MAMP install should be a simple double-click. On Mac, it will create a 'MAMP' 
folder under /Applications. On Windows, it will create a 'MAMP' folder at the root of 
your drive (typically C:\\).

Among other files and folders within the MAMP folder, you will find the MAMP application and the 'htdocs' folder. Copy all the files from the WordPress download (if it's a zip file, go ahead and 
unzip) into the 'htdocs' folder.

##### Configure WordPress

* Make a copy of 'wp-config-sample.php' file named 'wp-config.php' within the 'MAMP' folder
* Open 'wp-config.php' in Sublime Text
* Choose your own values for 'DB_NAME' and 'DB_USER' and 'DB_PASSWORD'
* On Windows, set 'DB_HOST' to the value 'localhost:3306'.  Windows seems to want us to specify the port for our database, which is 3306.
* Overwrite all the values under 'Authentication Unique Keys and Salts' with the values from https://api.wordpress.org/secret-key/1.1/salt/

##### Fire It Up!

Run the 'MAMP' application. It should display a small window with buttons:
* Preferences...
* Open WebStart page (disabled)
* Start Servers

Click the 'Start Servers' button.  Once both Apache and MySQL have started, the 
'Open WebStart page' button will enable.

##### Setup your database

Click the 'Open WebStart page' button.  This will open a webpage in your browser. You 
should see the URL http://localhost:8888/MAMP/ on Mac 
and http://localhost/MAMP/ on Windows.

Under the MySQL heading, there is a link named 'phpMyAdmin'.  Click the link to open a 
database manager. 

Let's create the database first:
* Click the 'Databases' tab
* Enter the database name you selected (the 'DB_NAME' value in wp-config.php) under Create database and click the 'Create' button

Now let's create the database user:
* Click the 'Privileges' tab.  Note that after creating the database, you will see a different set of tabs.
* Click the 'Add user account' link
* Enter the user name you selected (the 'DB_USER' value in wp-config.php) for 'User name'
* Enter 'localhost' for the 'Host name' value.  This is important for Windows users!
* Enter the password you selected (the 'DB_PASSWORD' value in wp-config.php) for 'Password' and 'Re-type'
* Click the check box for 'Grant all privileges on database ...' to allow this user to work with your database.

##### Install WordPress

Back on the WebStart page (http://localhost:8888/MAMP/ for Mac, http://localhost/MAMP/ for Windows), click 'My Website' from the top header bar.  This should open a page 
in your browser to complete the WordPress installation.  The URL should be http://localhost:8888/ on Mac.

```
Problems?

First, look at the bottom of wp-config.php and set the value for 'WP_DEBUG' to 'true'. 
This will provide you with better error messages to troubleshoot the issue.

You can find help from WordPress at https://codex.wordpress.org/Editing_wp-config.php
```

Select the language you wish to use (English is selected by default) and click the button to continue. 

Now we will create your WordPress admin user.  Select a name for your website, admin user name, and password.  You will see a confirmation screen.  Upon continuing, you 
will get to use your new user name and password to login to your new WordPress site!

Congratulations!

### Next Steps

##### Find your way around

Click around the WordPress admin site to get familiar with things.  Maybe try updating the Kismet plugin.  

Under 'Appearance' is a page for Themes. Notice that "Twenty Seventeen" is the selected theme.  Clicking 'Add New Theme' will take you to a theme browser, where you can select 
from hundreds of available themes.  You can also head 'offsite' to browse more themes (some that cost money) from websites like https://themeforest.net/.

##### Homework

Watch Codecademy Course on PHP
