### Session 8: WordPress Child Themes

##### Construction

```
style.css
```

You'll need the comment block.  Start by copying the header block from the theme you are creating a child theme for.  Update the name, version, and add the Template value to match the folder name of the parent theme.

```
functions.php
```

If you want to use styles from the parent theme along with styles in your child theme, you will want to copy the code snippit from https://codex.wordpress.org/Child_Themes which uses wp_enqueue_style() to add the parent style.css to your child theme.

##### Resources

* http://www.wpbeginner.com/beginners-guide/wordpress-child-theme-pros-cons/
* https://codex.wordpress.org/Child_Themes