### DCI Web Development & Design

This repository contains examples and notes used during our various sessions. 

Each session folder contains a README with an overview of what we covered.  This file basically contains the content of my session recap emails.