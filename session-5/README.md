### Session 5: WordPress Themes and Plugins

##### Lead with your content.  

This means:

* start simple.  you can always expand with fancier themes later
* identify your requirements, for today and anticipated in the future

Other suggestions for how you should optimize your site:

* treat security seriously.. see the security plugin recommendations below
* identify what support options you have available, in case you get stuck
* backup your work!  take 100% responsibility and don't simply rely on WordPress page/post versioning to save your bacon
* use MAMP to host a "staging" environment on your laptop
* premium themes may offer better support

##### Plugin recommendations

Security

* Force Strong Passwords
* All in One WP Security and Firewall
* Backup WordPress
* Akismet (bundled with your WP site)

SEO

* ALL in One SEO Pack

Forms

* Gravity Forms
* Contact Form 7
* HubSpot

Speed / Performance

* W3 Total Cache

Page Creators

* Thrive
* Themify Builder
* Visual Composer (this is often bundled with premium themes)

Hosting!  

* http://wpengine.com/ is the gold standard, but also expensive
* http://bluehost.com/ is recommended on wordpress.org and by Victor :)

##### Homework
* Get your hosting account activated
* Get a basic WP site installed on your host (some hosts offer a 1-click install option)
* Create a content map of your site.  Storyboard what pages and content you want to display
* (optional) Pick a theme.  So many to choose from, this can take some time.  Check out the gallery on http://wordpress.org/ or http://themeforest.net/
Happy developing!
-Ian