### Session 6: JavaScript Modals

##### What makes a modal

###### Trigger

This can be a link or button which controls when the modal is displayed.

###### Modal Dialog

This is the wrapping HTML tag which is designated as a modal.  It is hidden until the trigger is activated.

###### Modal Content 

This is where you place your HTML within the modal.  Some modals have special sections, like a header and footer. You can see an example in bootstrap-modal.html

##### Resources

* https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_modal
* https://getbootstrap.com
* http://jquerymodal.com